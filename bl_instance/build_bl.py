#!/usr/bin/env python
#######################################
import sys, os

bl_instance_path = '/home/experiences/proxima1/com-proxima1/olga/work/bl_monitoring/bl_instance'
print "bl_instance_path = ", bl_instance_path

from bl_model.bl_elems import *
####################################


u20 = motorized_beamModifier( 'u20',     # nickname
                              'ans-c10',  # vacuum_cell
                              'ei',      # dev_group
                              ['c-u20'], # devices
                              0.001      # delta
                              )

frontend = beam_stopper( 'frontend',                          # nickname
                         'tdl-i10-c/vi/tdl.1/frontEndStateValue', # beamAccess_attrFullname
                         [5, 6],                             # beamAccess_attrVals
                         0.1                                 # delta
                         )

slit1 = motorized_beamModifier( 'slit1',
                                'i10-c-c01',
                                'ex',
                                ['fent_h.1', 'fent_v.1'],
                                0.001
                                )

imag1 = extractable_beamMonitor( 'imag1',
                                 'i10-c-c01',
                                 'dt',
                                 ['imag1-analyzer', 'imag1-pos', 'imag1-vg'],
                                 'imag1-pos/isInserted'
                                 )

mono = motorized_beamModifier( 'mono',
                               'i10-c-c02',
                               'op',
                               ['mono1'],
                               0.0005
                               )

slit2 = motorized_beamModifier( 'slit2',
                                'i10-c-c02',
                                'ex',
                                ['fent_h.2', 'fent_v.2'],
                                0.001
                                )

xbpm1 = beam_monitor( 'xbpm1',         # nickname
                      'i10-c-c02',      # vacuum_cell
                      'dt',            # dev_group
                      #['xbpm_diode.1', 'xbpm_diode.1-pos']
                      ['xbpm_diode.1']  # devices
                      )

obx1 = beam_stopper( 'obx1',
                     'i10-c-c03/ex/obx.1/State',
                     ['OPEN']
                     )

xbpm2 = beam_monitor( 'xbpm2',
                       'i10-c-c03',
                       'dt',
                      #['xbpm_diode.2', 'xbpm_diode.2-pos']
                      ['xbpm_diode.2']
                      )

h_mir = motorized_beamModifier( 'h_mir',
                                'i10-c-c04',
                                'op',
                                ['mir1-mt_tx'],
                                0.001 # delta for Tx ??????????
                                )

v_mir = motorized_beamModifier( 'v_mir',
                                'i10-c-c04',
                                'op',
                                ['mir2-mt_tz'],
                                0.001 # delta for Tz   ??????????
                                )

att = motorized_beamModifier( 'att',
                              'i10-c-c04',
                              'ex',
                              ['att.1'],
                              0.01 # delta for transmission 
                              )

xbpm3 = beam_monitor( 'xbpm3',
                      'i10-c-c04',
                      'dt',
                      ['xbpm_diode.3']
                      )

slit3 = motorized_beamModifier( 'slit3',
                                'i10-c-c04',
                                'ex',
                                ['fent_h.3', 'fent_v.3'],
                                0.001
                                )

xbpm4 = beam_monitor( 'xbpm4',
                       'i10-c-c04',
                       'dt',
                      ['xbpm_diode.4']
                      )

xbpm5 = beam_monitor( 'xbpm5',
                       'i10-c-c04',
                       'dt',
                      ['xbpm_diode.5']
                      )

slit4 = motorized_beamModifier( 'slit4',
                                'i10-c-c04',
                                'ex',
                                ['fent_h.4', 'fent_v.4'],
                                0.001
                                )
#####################################################################################
beam_path = [
   frontend, 
   u20,
   slit1,
   mono,
   slit2,
   xbpm1, 
   obx1, 
   xbpm2,
   h_mir, v_mir, att, xbpm3,
   slit3, xbpm4,
   xbpm5, slit4
   ]
###################################################################################

ring_current = continuousEnvironment_elem('ring_current',
                                          'ans',
                                          'ca',
                                          ['machinestatus'],
                                          2000
                                          )

ring_beam = continuousEnvironment_elem('ring_beam',
                                       'ans-c10',
                                       'dg',
                                       ['calc-sdc-position-angle']
                                       )
tdl_beam = continuousEnvironment_elem('tdl_beam',
                                      'tdl-i10-c', 
                                      'dg',
                                      ['xbpm.1','xbpm.2']
                                      )

mono_temp1 = continuousEnvironment_elem('mono_temp1',
                                        'i10-c-c02', 
                                        'op',
                                        ['mono1-pt100.1'],
                                        5000
                                        )

mono_temp2 = continuousEnvironment_elem('mono_temp2',
                                        'i10-c-c02', 
                                        'ex',
                                        ['lash.1'],
                                        5000
                                        )

mir_temp = continuousEnvironment_elem('mir_temp',
                                      'i10-c-c04', 
                                      'vi',
                                      ['tc.1'],
                                      5000
                                      )

gonio_temp = continuousEnvironment_elem('gonio_temp',
                                        'i10-c-cx1', 
                                        'vi',
                                        ['tc.1'],
                                        5000
                                        )

continuous_env = [
   ring_current, 
   ring_beam,
   tdl_beam,
   mono_temp1,
   mono_temp2,
   mir_temp,
   gonio_temp
   ]


###################################
if __name__ == '__main__':
   
   xbpm2.define_blSteadiness(beam_path )
###
   for elem in xbpm2.blSteadiness_elems:
      elem.print_params()
   for p in xbpm2.beamAccess_params:
      print p.attr_fullName

