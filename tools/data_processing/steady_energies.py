#!/usr/bin/env python
##################################
import os, sys, time

import numpy

from bl_instance.build_bl import *

from tools.generic_utils.time_formats  import *
from tools.generic_utils.time_intervals  import *
from timePortioning_byData import *

from tools.archiving_extraction import paramdata_filename
#################################




###############################################
##startShift_hour = '07:00:00'
##endShift_hour = '07:00:00'

steadyPeriod_minLength_sec = 60*10 # = 10 min
##EXTRAPERIOD_MAXLENGTH_SEC = 60*60*3 # = 3 hours

ENERGY_ATTR = 'mono1/energy'
###############################################



############################
if __name__ == '__main__':


    if len(sys.argv) < 2:
        print "Correct use: " + sys.argv[0] + " date_str('Y-m-d')  [data_path]"
        sys.exit(0)

    date_str = sys.argv[1]
    year, month, day  =  date_str.split('-')

    data_path = bl_instance_path + os.sep + 'extracted_data'
    if len(sys.argv) == 3:
        data_path = sys.argv[2]
        

    # Find the energy attribute in the beam_path ---
    
    attr_found = False
    for elem in beam_path:
        #print elem.nickname # debug

        attrs_params = dict( [(p.name, p) for p  in (elem.params)] )
        #print attrs_params.keys() # debug
        
        if ENERGY_ATTR in attrs_params.keys():
            
            param = attrs_params[ENERGY_ATTR]
            attr_found = True
            break
            
    if not attr_found:
        print "Invalid ENERGY_ATTR"
        sys.exit(0)


    

    month_dir  = data_path + os.sep + year + os.sep + month
    archived_dates = os.listdir(month_dir)
    #print archived_dates  # debug

    
    if not day in archived_dates:
        print " No data for ", date_str
        sys.exit(0)

    day_dir = month_dir + os.sep + day
    #os.chdir(day)
    data_files = os.listdir(day_dir)

    for f in data_files:
        if ENERGY_ATTR.replace('/', '.') in f:
            energy_datafile = f
            break
    #print energy_datafile # debug
    # supposing there is only one datafile for energy
    # otherwise, we would list them


    param_data  = (numpy.loadtxt( day_dir + os.sep + energy_datafile, usecols=(0,1), unpack=True )) # as 2 colomns


    data_timeInterval_str = paramdata_filename.filename2timeInterval(day_dir + os.sep + energy_datafile)
    #print data_timeInterval_str # debug


    param_data[0] = param_data[0]+ str2sec(data_timeInterval_str[0])

    param_data = param_data.transpose()


    steadyPeriods_str, param_vals = get_steadyPeriods(param, param_data, data_timeInterval_str, steadyPeriod_minLength_sec)
    print param_vals
    print steadyPeriods_str
    
    shifted_steadyPeriods_sec = shift_time( periodsStr2periodsSec(steadyPeriods_str), - str2sec(data_timeInterval_str[0]) )
    print shifted_steadyPeriods_sec
     # no eshe nado sootvetstvuyushie znachenija energy...
