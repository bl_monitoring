#!/usr/bin/env python

import sys
import os

needed_dirs = ['/home/roudenko/work/bl_monitoring/']

for needed_dir in needed_dirs:
    if os.path.exists(needed_dir):
        if not needed_dir in sys.path:
            sys.path.append(needed_dir)
    
