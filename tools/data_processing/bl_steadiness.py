#!/usr/bin/env python
##################################
import os, sys, time
import numpy
import sets

import numpy

import home_prefix

#from bl_instance.build_bl import *

from tools.generic_utils.time_formats  import *
from tools.generic_utils.time_intervals  import *
from tools.archiving_extraction import paramdata_filename
from splittingTime_byData import *

###################################
from bl_instance.build_bl import *
###################################


###############################################
##startShift_hour = '07:00:00'
##endShift_hour = '07:00:00'

STEADYPERIOD_MINLENGTH = 60*10 # = 10 min
#steadyPeriod_maxLength_sec = 60*60*3 # = 3 hours
###############################################


#################
class bl_steady:
    
    def __init__(self, _steady_params, _rel_timeInterval_sec, _abs_timeInterval_str, _monitor = 0):

        self.steady_params = _steady_params       # dict{name:val}
        self.rel_timeInterval = _rel_timeInterval_sec
        self.abs_timeInterval = _abs_timeInterval_str
        self.monitor = _monitor # is needed to order params following the beam_path


    def __repr__(self):

        params_str = ''

        if self.monitor:
            params_in = True
            for p in self.monitor.beamAccess_params + self.monitor.blSteadiness_params:
                if not p.name in self.steady_params.keys():
                    params_in = False
                    break
                
            if params_in:
                for p in self.monitor.beamAccess_params + self.monitor.blSteadiness_params:
                    params_str += p.name + ' = ' + str(self.steady_params[p.name]) + '\n'
            else:
                for p_name in self.steady_params.keys():
                    params_str += p_name + ' = ' + str(self.steady_params[p_name]) + '\n'
        else:
            for p_name in self.steady_params.keys():
                params_str += p_name + ' = ' + str(self.steady_params[p_name]) + '\n'
            
        return ( '\n'+ str(self.rel_timeInterval) + ' -> ' + str(self.abs_timeInterval) + '\n'
                 + params_str )



    def matches_condition(self, param_name, param_val, delta = -1):

        if not param_name in self.steady_params.keys():
            print "Invalid param name in bl_steady.matches_condition"
            sys.exit(0)
            
        if delta == -1:
            delta = 0

        return abs(self.steady_params[param_name] - param_val) <= delta




###########################################################
def get_blSteadyStates(params_data, overAll_timeInterval_str):

    params_breakMoments_vals = {} ###
    latest_firstMoment = 0  # latest first data over all params
    
    for p in params_data.keys():
        print p.name # DEBUG

        if params_data[p][0][0] > latest_firstMoment:
            latest_firstMoment = params_data[p][0][0]
        
        params_breakMoments_vals[p] = get_breakMoments_vals(p, params_data[p])
        print numpy.transpose(params_breakMoments_vals[p]) # DEBUG
        
        'params_breakMoments_vals[p][0] = vector of moments (rel. time in sec.)'
        'params_breakMoments_vals[p][1] = vector of corresponding values'

    print '\nlatest_firstMoment = ', latest_firstMoment, '\n' # DEBUG
    
    for p in params_breakMoments_vals.keys():
        p_n = len(params_breakMoments_vals[p][0])
        m_inx = 0
        if p_n:
            while (params_breakMoments_vals[p][0][m_inx] < latest_firstMoment and
                   m_inx < p_n -1):
                m_inx += 1

            
            if m_inx != 0: 
                
                first_val = params_breakMoments_vals[p][1][m_inx-1] # keep this val for each param
                
                #for all params, remove all the stuff (moments and vals) preceeding the latest_firstMoment:
                del params_breakMoments_vals[p][0][:m_inx]
                del params_breakMoments_vals[p][1][:m_inx]

                params_breakMoments_vals[p][0].insert(0, latest_firstMoment)
                params_breakMoments_vals[p][1].insert(0, first_val)
                
            else:
                params_breakMoments_vals[p][0].insert(0, latest_firstMoment)
                params_breakMoments_vals[p][1].insert(0, params_data[p][0][1])
                
        else: 
            params_breakMoments_vals[p][0].append(latest_firstMoment)
            params_breakMoments_vals[p][1].append(params_data[p][0][1])

        
    overAll_breakMoments = []  ### 
    for p in params_breakMoments_vals.keys():
        overAll_breakMoments += params_breakMoments_vals[p][0]
        
    overAll_breakMoments = list(sets.Set(overAll_breakMoments))# removes duplicates
    overAll_breakMoments.sort()
    print "overAll_breakMoments = ", overAll_breakMoments, '\n' # DEBUG
        
    params_steadyVals = {} ### 
    for p in params_breakMoments_vals.keys():

        p_valsNb = len(params_breakMoments_vals[p][0])
        
        
        params_steadyVals[p] = [params_breakMoments_vals[p][1][0]] # init: corresponding to the latest_firstMoment
        
        overAll_i = 1 # 0 corresponds to the latest_firstMoment for all params
        for p_i in range(1, p_valsNb):
            
            while overAll_breakMoments[overAll_i] < params_breakMoments_vals[p][0][p_i]:
                params_steadyVals[p].append(params_breakMoments_vals[p][1][p_i-1]) 
                overAll_i += 1

        def f(x): return params_breakMoments_vals[p][1][p_valsNb-1]

        params_steadyVals[p] += map(f, range( overAll_i,len(overAll_breakMoments) ))
            
        print "params_steadyVals[", p.name, "] = ", params_steadyVals[p] # DEBUG
        
    #print params_steadyVals # DEBUG
                
    bl_steadyStates = []
    for mi in range(len(overAll_breakMoments)-1):

        # if the interval is sufficiently long
        if overAll_breakMoments[mi+1] - overAll_breakMoments[mi] >= STEADYPERIOD_MINLENGTH:

            steady_params = {}
            
            curr_timeInterval_rel = (overAll_breakMoments[mi], overAll_breakMoments[mi+1])

            curr_timeInterval_abs = rel2abs([curr_timeInterval_rel], overAll_timeInterval_str[0])[0]
            
            for p in params_breakMoments_vals.keys():
                #print mi, ' ', params_steadyVals[p][mi]
                steady_params[p.name] = params_steadyVals[p][mi]

            bl_steadyStates.append( bl_steady(steady_params, curr_timeInterval_rel, curr_timeInterval_abs, monitor) )
            #print bl_steadyStates[-1] # debug
        
    # and at last ---
    last_moment = str2sec(overAll_timeInterval_str[1]) - str2sec(overAll_timeInterval_str[0])
    if last_moment - overAll_breakMoments[-1] >= STEADYPERIOD_MINLENGTH:

        steady_params = {}

        last_timeInterval_rel = (overAll_breakMoments[-1], last_moment)
        last_timeInterval_abs = rel2abs([last_timeInterval_rel], overAll_timeInterval_str[0])[0]
        
        for p in params_breakMoments_vals.keys():
            steady_params[p.name] = params_steadyVals[p][-1]

        bl_steadyStates.append( bl_steady(steady_params, last_timeInterval_rel, last_timeInterval_abs, monitor) )
        #print bl_steadyStates[-1] # debug
    # ---

    return bl_steadyStates
            
    
############################
if __name__ == '__main__':


    if len(sys.argv) < 2:
        print "Correct use: " + sys.argv[0] + " date_str('Y-m-d')  [data_path]"
        sys.exit(0)

    date_str = sys.argv[1]
    year, month, day  =  date_str.split('-')

    data_path = bl_instance_path + os.sep + 'extracted_data'
    if len(sys.argv) == 3:
        data_path = sys.argv[2]
        
    month_dir  = data_path + os.sep + year + os.sep + month
    archived_dates = os.listdir(month_dir)
    #print archived_dates  # debug

    
    if not day in archived_dates:
        print " No data for ", date_str
        sys.exit(0)

    day_dir = month_dir + os.sep + day
    #os.chdir(day)
    data_files = os.listdir(day_dir)


    for elem in beam_path:
        if isinstance(elem, beam_monitor)and not isinstance(elem, extractable_beamMonitor):
        # until the first beam monitor
        
            monitor = elem # just a notation for better readability
            #print '\nMonitor: ', monitor.nickname  # DEBUG

            monitor.define_blSteadiness(beam_path)

            params_data = {}             ###
            for param in monitor.beamAccess_params + monitor.blSteadiness_params:
                #print param.name # DEBUG
                
                for f in data_files:
                    if param.name.replace('/', '.') in f:
                        param_datafile = f
                        break
                # supposing there is only one datafile for each param

                #print param_datafile # DEBUG
                
                param_data  = (numpy.loadtxt( day_dir + os.sep + param_datafile, usecols=(0,1), unpack=True )) # as 2 colomns
                param_data = param_data.transpose()
                #print len(param_data)
                
                
                overAll_timeInterval_str = paramdata_filename.filename2timeInterval(day_dir + os.sep + param_datafile)
                #print overAll_timeInterval_str # DEBUG

                params_data[param] = param_data

            bl_steadyStates = get_blSteadyStates(params_data, overAll_timeInterval_str)

            print bl_steadyStates

##            for s in bl_steadyStates:
##                #if s.matches_condition( 'mono1/energy', 12.65, 0.0005):
##                if s.matches_condition( 'tdl.1/frontEndStateValue', 5.):
##                    print s
            
            
            break # only until the first beam monitor
