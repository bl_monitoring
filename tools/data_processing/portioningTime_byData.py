#!/usr/bin/env python
#
import os, sys, time

from tools.generic_utils.time_formats import *
from tools.generic_utils.time_intervals import *
#from tools.generic_utils.dataPortioning_byTime import *

from  splittingTime_byData import get_breakMoments_vals

#################################################################################
def get_extraPeriods_sec(param, param_data, timeInterval_sec, minLength_sec = 0):

    if param.ok_values == 'const':
        print 'In portioningTime_byData.get_extraPeriods: Invalid argument param'
        sys.exit(0)

    break_moments, break_vals = get_breakMoments_vals(param, param_data)
    
    print "In get_extraPeriods_sec: len(break_moments) = ", len(break_moments) # debug


    if len(break_moments) == 0:
        if param.is_ok(param_data[0][0]):
            return [(param_data[0][0], timeInterval_sec[1])]
        else:
            return []


    if param.is_ok(break_vals[0]):
        
        if len(break_moments) == 1:
            return [(break_moments[0], timeInterval_sec[1])]
        else:
            ok_intervals_sec = []
            
            for i in range(0, len(break_moments)-1, 2):
                ok_intervals_sec.append( (break_moments[i], break_moments[i+1]) )
                
    else: # the first break moment passes from ON to OFF
        ok_intervals_sec = [(param_data[0][0], break_moments[0])]

        for i in range(1, len(break_moments)-1, 2):
            ok_intervals_sec.append( (break_moments[i], break_moments[i+1]) )

    if ok_intervals_sec[-1][1] == break_moments[-2]: 
        ok_intervals_sec.append( (break_moments[-1], timeInterval_sec[1]) )

    return select_longIntervals(ok_intervals_sec, minLength_sec)
                                                         

#################################################################################
def takeWholeOrEmpty_extendingTime(param, timeInterval_sec, param_data):

    if ( timeInterval_sec[1] - timeInterval_sec[0] <=
             float(param.archiving_period)/1000 ):
            
            extended_timeInterval_sec = ( max( timeInterval_sec[1]- float(param.archiving_period)/1000,
                                               param_data[0][0]),
                                          timeInterval_sec[1] )

            # take  the corresponding portion of param_data
            data_portion = get_dataPortion(param_data, extended_timeInterval_sec)
            
            if len(data_portion) == 0:
                print "No data for ", param.attr_fullName, " in the ", db, \
                      " for the period ", periodsSec2periodsStr([extended_timeInterval_sec])[0]
                sys.exit(0)

            if param.is_ok(data_portion[-1][1]):
                return [timeInterval_str]      # whole interval
            else:
                return []                      # empty list
    else:
        print "No data for ", param.attr_fullName, " in the ", db, \
              " for the period ", periodsSec2periodsStr(timeInterval_sec)
        sys.exit(0)


