#!/usr/bin/env python
#
import os, sys, time

from tools.generic_utils.time_formats import *
from tools.generic_utils.time_intervals import *
#from tools.generic_utils.dataPortioning_byTime import *


#############################################
def get_okIntervals_indxs(param, param_data):

    if len(param_data) == 0:
        return [] # it is not really correct but we are not supposed to get here 
      
    #timeInterval_sec = periodsStr2periodsSec([timeInterval_str])[0]

    curr_startInx = -1
    curr_endInx = -1
    ok_intervals = []
    was_ok = False

    for i in range(len(param_data)):

        ok = param.is_ok(param_data[i][1])
        if ok and not was_ok:
            curr_startInx = i
            was_ok = True
        elif not ok and was_ok:
            curr_endInx = i-1
            was_ok = False
            ok_intervals.append((curr_startInx, curr_endInx))
            
    #if curr_startInx != -1 and curr_endInx == -1:
    if curr_startInx > curr_endInx:
        ok_intervals.append((curr_startInx, len(param_data)-1))

    #print ok_intervals # debug

##    if len(ok_intervals) == 0:
##        return []

    return ok_intervals


#################################################################################
def get_extraPeriods(param, param_data, timeInterval_str, minLength_sec = 0):


    okIntervals_indxs = get_okIntervals_indxs(param, param_data)

    if len(okIntervals_indxs) == 0:
        return []

    subIntervals_sec = []

    timeInterval_sec = periodsStr2periodsSec([timeInterval_str])[0]
    
    if okIntervals_indxs[0][0] == 0:
        first_start = timeInterval_sec[0]
    else: first_start = param_data[okIntervals_indxs[0][0]][0]

    #print sec2str(first_start) # debug
    
    if okIntervals_indxs[-1][1] == len(param_data)-1:
        last_end = timeInterval_sec[1]
    else: last_end = param_data[okIntervals_indxs[-1][1]][0]

    #print sec2str(last_end) # debug

    if len(okIntervals_indxs) == 1:
        subIntervals_sec.append( (first_start,  last_end) )
    else:
        subIntervals_sec.append((first_start,  param_data[okIntervals_indxs[0][1]][0]))
        for i in range(1, len(okIntervals_indxs)-1):
            subIntervals_sec.append((param_data[okIntervals_indxs[i][0]][0],  param_data[okIntervals_indxs[i][1]][0]))
        subIntervals_sec.append((param_data[okIntervals_indxs[-1][0]][0],  last_end))


    
    return periodsSec2periodsStr( select_longIntervals(subIntervals_sec, minLength_sec) )


#################################################################################
def get_steadyPeriods(param, param_data, timeInterval_str, minLength_sec = 0):

    #subIntervals_sec = periodsStr2periodsSec([timeInterval_str])

    var_data = param.get_variation(param_data, periodsStr2periodsSec([timeInterval_str])[0])
    
    okIntervals_indxs = get_okIntervals_indxs(param, var_data)

    subIntervals_sec = []
    param_vals = []
    for i in okIntervals_indxs:
        subIntervals_sec.append( (var_data[i[0]][0][0], var_data[i[1]][0][1]) )
        param_vals.append(param_data[i[0]][1])
        
    #return periodsSec2periodsStr( select_longIntervals(subIntervals_sec, minLength_sec) ), param_vals
    return periodsSec2periodsStr(subIntervals_sec), param_vals

#################################################################################
def takeWholeOrEmpty_extendingTime(param, timeInterval_str, param_data):

    if ( str2sec(timeInterval_str[1]) - str2sec(timeInterval_str[0]) <=
             float(param.archiving_period)/1000 ):
            
            extended_timeInterval_sec = ( max( str2sec(timeInterval_str[1])- float(param.archiving_period)/1000,
                                               param_data[0][0]),
                                          str2sec(timeInterval_str[1]) )

            # take  the corresponding portion of param_data
            data_portion = get_dataPortion(param_data, extended_timeInterval_sec)
            
            if len(data_portion) == 0:
                print "No data for ", param.attr_fullName, " in the ", db, \
                      " for the period ", periodsSec2periodsStr([extended_timeInterval_sec])[0]
                sys.exit(0)

            if param.is_ok(data_portion[-1][1]):
                return [timeInterval_str]      # whole interval
            else:
                return []                      # empty list
    else:
        print "No data for ", param.attr_fullName, " in the ", db, \
              " for the period ", timeInterval_str
        sys.exit(0)


