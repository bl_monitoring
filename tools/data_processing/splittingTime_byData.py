#!/usr/bin/env python
#
import os, sys, time

from tools.generic_utils.time_formats import *
from tools.generic_utils.time_intervals import *
#from tools.generic_utils.dataPortioning_byTime import *


#####################################
def get_breakIndxs(param, param_data):


##    was_ok = False
##    break_indxs = []
    
##    for i in range( len(param_data) ):

##        ok = param.is_ok(param_data[i][1])

##        if param.ok_values == ['const']:
##            if not ok:
##                break_indxs.append(i)

##        else:
##            if (ok and not was_ok) or (not ok and was_ok):
##                break_indxs.append(i)
##                was_ok = not was_ok


    break_indxs = []

    if param.ok_values == ['const']:

         for i in range( len(param_data) ):
             
             if not param.is_ok(param_data[i][1]):
                 break_indxs.append(i)

    else:
        was_ok = param.is_ok(param_data[0][1])
        print "In get_breakIndxs: was_ok = ", was_ok # debug
        
        for i in range( 1,len(param_data) ):
            
            ok = param.is_ok(param_data[i][1])

            if (ok and not was_ok) or (not ok and was_ok):
                break_indxs.append(i)
                was_ok = not was_ok

    return break_indxs 


############################################
def get_breakMoments_vals(param, param_data): # returns a dictionary 

    #breakMoments_vals = {}
    break_moments = []
    vals = []

    if param.ok_values == ['const']:
        var_data = param.get_variation(param_data, (param_data[0][0],param_data[-1][0]))
    
        break_indxs = get_breakIndxs(param, var_data)

    else:
        break_indxs = get_breakIndxs(param, param_data)



    for bi in  break_indxs:
        #breakMoments_vals[param_data[bi][0]-1] = param_data[bi][1]
        break_moments.append(param_data[bi][0]) # sec
        vals.append(param_data[bi][1])
    
    #return breakMoments_vals # dict
    return break_moments, vals


