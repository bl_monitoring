#!/usr/bin/env python
#
import os, sys, time

from PyTango import *

from build_bl import beam_path
from bl_metastruct import *

from extraction import *
from data_portioning import *


###############################################
startShift_hour = '08:00:00'
endShift_hour = '08:00:00'

extraPeriod_minLength_sec = 60*5 # = 5 min
EXTRAPERIOD_MAXLENGTH_SEC = 60*60*3 # = 3 hours

STEADYPERIODS_SEPARATORS = ['mono1/energy']
###############################################



############################
if __name__ == '__main__':


    if len(sys.argv) < 3:
        print "Correct use: " + sys.argv[0] + " startDate_str ('Y-m-d')  dest_dir  [hdb/tdb]"
        sys.exit(0)

    startDate_str = sys.argv[1]
    dest_dir = sys.argv[2]

    
    db = "TDB"
    if len(sys.argv) == 4 and (sys.argv[3] == "hdb" or sys.argv[3] == "HDB"):
        extractor = DeviceProxy("archiving/hdb/hdbextractor.1")
        db = "HDB"
    else:
        extractor = DeviceProxy("archiving/tdb/tdbextractor.1")
    #print db #debug


    extractor.set_timeout_millis(100000)
    # ----------------------------------------------------------------------------------------------

    startShift_str = startDate_str +' '+ startShift_hour
    startShift_sec = str2sec(startShift_str)

    startShift_hour_sec =  hour2sec(startShift_hour)
    endShift_hour_sec =  hour2sec(endShift_hour)
    
    if startShift_hour_sec >= endShift_hour_sec:
        startShift_date_sec = time.mktime(time.strptime(startDate_str, "%Y-%m-%d"))
        endShift_date_sec = startShift_date_sec + hour2sec("24:00:00")

    endShift_sec = endShift_date_sec + endShift_hour_sec
    endShift_str = sec2str(endShift_sec) 

##    print startShift_str # debug
##    print endShift_str # debug
    # ----------------------------------------------------------------------------------------------
    
    # INIT ---
    extractionPeriods_sec = [(startShift_sec, endShift_sec)]
    extractionPeriods_str = periodsSec2periodsStr(extractionPeriods_sec)
    steadyPeriods_str = extractionPeriods_str

    #beam_monitors = []
    #############################    Main loop    #########################

    for elem in beam_path:
        if isinstance(elem, beam_monitor)and not isinstance(elem, extractable_beamMonitor):
            monitor = elem # just a notation for better readability
            print '\nMonitor: ', monitor.nickname
            #beam_monitors.append(elem)
            
            monitor.define_blSteadiness(beam_path)

            # ------------------------------------------
            for param in monitor.beamAccess_params:
                print '\nBeam access: ', param.name # debug

                new_extractionPeriods_str = []
                for time_interval in extractionPeriods_str:
                    #print timeInterval_str # debug

                    param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                           time_interval,  extractor, dest_dir )
                    #print len(param_data) # debug
                    if len(param_data) == 0:
                        subPeriods_str =  takeWholeOrEmpty_extendingExtraction(param, time_interval, extractor, dest_dir)
                    else: 
                        subPeriods_str = get_extraPeriods(param, param_data, time_interval, extraPeriod_minLength_sec)
                        
                    new_extractionPeriods_str += subPeriods_str
                    #print new_extractionPeriods_str # debug

                extractionPeriods_str = new_extractionPeriods_str
                print extractionPeriods_str # debug
                
            steadyPeriods_str = extractionPeriods_str
            #print steadyPeriods_str # debug

            # ------------------------------------------
##            steadinessParam_names = []
##            for param in monitor.blSteadiness_params:
##                steadinessParam_names.append(param.name)
##            print steadinessParam_names # debug

            for param in monitor.blSteadiness_params:
                if param.name in STEADYPERIODS_SEPARATORS:
                    print '\nSteadiness: ', param.name # debug
                    
                    new_steadyPeriods_str = []
                    
                    for time_interval in extractionPeriods_str:

                        param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                               time_interval,  extractor, dest_dir )
                        if len(param_data) == 0:
                            subPeriods_str = [time_interval] # as no value means it did not change => steadiness
                            # ........... but the extension of the time_interval may be needed anyway ? .......
                        else:
                            subPeriods_str = get_steadyPeriods(param, param_data, time_interval,
                                                               extraPeriod_minLength_sec)
                        new_steadyPeriods_str += subPeriods_str
                        #print new_steadyPeriods_str  # debug
                        
                    steadyPeriods_str = new_steadyPeriods_str
                    print steadyPeriods_str

            # ------------------------------------------
            for param in monitor.blSteadiness_params:
                if not param.name in STEADYPERIODS_SEPARATORS:
                    
                    for steady_period in steadyPeriods_str:
##                        shortSteadyPeriods_str = split_longInterval(steady_period, EXTRAPERIOD_MAXLENGTH_SEC)

##                        for short_steadyPeriod in shortSteadyPeriods_str:
                            
                            param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                                   steady_period,  extractor, dest_dir )
                        
            # ------------------------------------------
            for param in monitor.params:
                print '\n', param.name # debug
                
                for steady_period in steadyPeriods_str:
                    shortSteadyPeriods_str = split_longInterval(steady_period, EXTRAPERIOD_MAXLENGTH_SEC)
                    
                    for short_steadyPeriod in shortSteadyPeriods_str:
                        
                        param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                               short_steadyPeriod,  extractor, dest_dir )
            # -------------------------------------------------------
            
            sys.exit(0) ############ DBUG ##############
    
        



    # TEST 1 -------------------------------------------
    attr_list = reading_txtac.attrs_fromDevList(archi_filename)
    #print attr_list # DEBUG
    date_string = startTime_str[:10]
    startHour_string = startTime_str[11:].replace(':', '.')
    endHour_string = endTime_str[11:].replace(':', '.')
    
    baseOutputName = dest_dir + os.sep + date_string + '_' +  startHour_string + '-' + endHour_string + '_'
    
    extractData_fromTimeToTime( attr_list, startTime_str, endTime_str, extractor, baseOutputName)
  
         
    # TEST 2 -------------------------------------------
    #extractData_archivedPeriods(archi_filename,  startTime_str, endTime_str, extractor, dest_dir)
 		

