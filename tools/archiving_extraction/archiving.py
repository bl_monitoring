#!/usr/bin/env python

#########################
import sys, os, time

from PyTango import *

from archi_utils import sort_by_archiModes
##############################################



def archiving(attrs_archiModes, action,  db = 'Tdb'):  
##############################################################################################

    if action != 'start' and action != 'stop' and action != 'check':
        print "\nCorrect use: " + "archving_*   start/stop/check   BEAM_MONITOR_ID/DEV_FULLNAME/ATTR_FULlNAME   [tdb/hdb]\n"
        sys.exit(0)

    if db != 'Tdb' and action != 'Hdb': # TODO: case-indifferent check
        print "Invalid db argument passed to the function 'archiving'"
        sys.exit(0)

    check_command = "IsArchived" + db
    if action in ['start', 'stop']:
        action_command = "Archiving" + action[:1].upper()+action[1:] + db
        #print "DEBUG: action_command = " + action_command

    
    ac_byModes = sort_by_archiModes(attrs_archiModes)
    #print ac_byModes # debug

    #sys.exit(0) ###########################################

    all_startArchivingArgs = []
    all_attrLists = []
    for m in range (len(ac_byModes)):

        startArchivingArgs_m = [] 
        
        startArchivingArgs_m.append('0') # are all attrs archived in the same HdbArchivier device
        
        m_attrsNb =  len(ac_byModes[m][1])
        startArchivingArgs_m.append( m_attrsNb ) # num of attrs to archive
        for a in range (m_attrsNb):
            startArchivingArgs_m.append( ac_byModes[m][1][a] ) # list of the archived attrs
        #print startArchivingArgs_m # debug
        for p in range ( len(ac_byModes[m][0]) ):
            startArchivingArgs_m.append( ac_byModes[m][0][p] ) # archiving mode parameters
        #print ac_byModes[m][0] # debug
            
        if db == "Tdb":
            startArchivingArgs_m.append('TDB_SPEC') # supplimentary params for the temporary archiving
            startArchivingArgs_m.append('1800000') # bo
            startArchivingArgs_m.append('21600000') # bo

        #print "\nArchiving configuration " + str(m+1)  # debug
        #print startArchivingArgs_m # debug
        
        all_startArchivingArgs.append(startArchivingArgs_m)

        all_attrLists.append(startArchivingArgs_m[2:m_attrsNb+2])
        #print all_attrLists[m] # debug

    if action == 'start':
        actionCommand_args = all_startArchivingArgs
    else:
        actionCommand_args = all_attrLists
    #print "DEBUG: len(actionCommand_args) = " +  str(len(actionCommand_args))

    #sys.exit(0) ##################################################################


    archivingmanager = DeviceProxy('archiving/archivingmanager/archivingmanager.1')
    archivingmanager.set_timeout_millis(30000)

    
    for m in range (len(all_startArchivingArgs)):

        #mode = extractor.GetArchivingMode( blSteadiness_attrNames[a] )

        print "\nArchiving configuration " + str(m+1)  # debug
        print all_startArchivingArgs[m] # debug
        loggingStatus_list = archivingmanager.command_inout(check_command, all_attrLists[m])
        print "\nArchiving configuration " + str(m+1) + " status:  " + str(loggingStatus_list)

        if action == 'check':
            extractor = DeviceProxy('archiving/tdb/tdbextractor.1')
            for a in range (len(all_attrLists[m])):
                if loggingStatus_list[a] == 0:
                    print all_attrLists[m][a], " is not being archived"
                else:
                    actual_archiMode = extractor.GetArchivingMode(all_attrLists[m][a])
                    print actual_archiMode  # debug
                    model_archiMode = attrs_archiModes[all_attrLists[m][a]]
                    #print model_archiMode  # debug

                    same_mode = True
                    if  len(actual_archiMode) < len(model_archiMode):
                        same_mode = False
                    else:
                        for i in range (len(model_archiMode)):
                            if actual_archiMode[i] != model_archiMode[i]:
                                if '.' in model_archiMode[i]:
                                    if float(actual_archiMode[i]) != float(model_archiMode[i]):
                                        print float(actual_archiMode[i])
                                        print float(model_archiMode[i])
                                        same_mode = False
                                        break
                                else:
                                    same_mode = False
                                    break
                    if not same_mode:
                        print all_attrLists[m][a], " is being archived with the following archiving mode:"
                        print actual_archiMode
                                


    if action == 'start':
        print "\n" + str( time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) ) + '\t' + "Starting archiving"
    if action == 'stop':
        print "\n" + str( time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) ) + '\t' + "Stopping archiving"


    if action in ['start', 'stop']:
        
        for m in range (len(actionCommand_args)):

            print 
            print action_command
            print actionCommand_args[m]
            archivingmanager.command_inout(action_command, actionCommand_args[m])
        
            loggingStatus_list = archivingmanager.command_inout(check_command, all_attrLists[m])
            
            if(action == 'start' and sum(loggingStatus_list) != len(loggingStatus_list)):
                
                print '\nWARNING: Archiving has not been correctly started'
                print "Archiving configuration " + str(m+1)  
                print actionCommand_args[m] 
                
            if(action == 'stop' and sum(loggingStatus_list) != 0):
                
                print '\nWARNING: Archiving has not been correctly stopped'
                print "\nArchiving configuration " + str(m+1)  
                print actionCommand_args[m]
                
            print "\nArchiving configuration " + str(m+1) + " status:  " + str(loggingStatus_list) 
