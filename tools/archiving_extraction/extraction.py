#!/usr/bin/env python
#
import os, sys, time

from PyTango import *

from paramdata_filename import timeInterval2paramdataFilename
#############################################################################

def attrData_fromTimeToTime(attr_name, startTime_str, endTime_str, extractor):
    
    extraction_args = []
    extraction_args.append( attr_name)
    extraction_args.append( startTime_str)
    extraction_args.append( endTime_str)

    attr_data = []
    if ( extractor.GetAttDataBetweenDatesCount(extraction_args) ):
        
        attrData_pointer = extractor.GetAttDataBetweenDates( extraction_args )

        valsNb = attrData_pointer[0][0]
        buffer_id = attrData_pointer[1][0]
        
        #print 'attrData_fromTimeToTime: valsNb = ' + str(valsNb)
        #print 'attrData_fromTimeToTime: buffer_id = ' + str(buffer_id)

        for data_item in extractor.attribute_history( buffer_id, valsNb ):
            attr_data.append( (data_item.value.time.tv_sec, data_item.value.value) )
            
        attr_data.sort() # !!!!!!!!!


        "The dynamic attribute created by the GetAttDataBetweenDates commande is to be removed"
        if attrData_pointer != []:
            extractor.RemoveDynamicAttribute( buffer_id )

    return attr_data


##################################################################################################
def extractAttr_fromTimeToTime(attr_name, startTime_str, endTime_str, extractor, output_filename ):

    output_file = open(output_filename, 'w')
    
    attr_data = attrData_fromTimeToTime(attr_name, startTime_str, endTime_str, extractor)

    start_time = time.mktime(time.strptime(startTime_str, "%Y-%m-%d %H:%M:%S"))

    for i in range (len(attr_data)):
        output_file.write( str(attr_data[i][0]-start_time) + "   "  + str(attr_data[i][1]) + "\n")

        
#################################################################################################
def extractAttr_relativeTime(attr_name, zeroTime_sec, extraPeriod_str, extractor, dest_dir = '.' ):

    attr_data = attrData_fromTimeToTime(attr_name,
                                        extraPeriod_str[0], extraPeriod_str[1], extractor)


    #-------------------- filename --------------------
##    date_str = extraPeriod_str[0][8:10]
##    startHour_str = extraPeriod_str[0][11:].replace(':', '.')
##    endHour_str = extraPeriod_str[1][11:].replace(':', '.')
    
##    dev_name = attr_name.split('/')[-2]
##    attr_shortName = attr_name.split('/')[-1]

##    output_filename = ( dest_dir + os.sep +  date_str +'_'+ startHour_str + '-' + endHour_str + '_'
##                        + dev_name + '.' + attr_shortName )
    #----------------------------------------------------

    output_filename =  dest_dir + os.sep +  timeInterval2paramdataFilename(attr_name, extraPeriod_str)

    output_file = open(output_filename, 'w')

    for i in range (len(attr_data)):
        output_file.write( str(attr_data[i][0]-zeroTime_sec) + "   "
                           + str(attr_data[i][1]) + "\n")

    return attr_data

##################################################################################################
##def extractData_fromTimeToTime(attr_list, startTime_str, endTime_str, extractor, baseOutputName):

##    for i in range ( len(attr_list) ):
##        attr_fulName = attr_list[i]
##        devName = attr_fulName.split('/')[-2]
##        attr_shortName = attr_fulName.split('/')[-1]
##        #print attr_shortName
##        filename_i = baseOutputName + devName + '.' + attr_shortName 
##        extractAttr_fromTimeToTime(attr_fulName, startTime_str, endTime_str, extractor, filename_i)





##################################################################################################
if __name__ == '__main__':


    if len(sys.argv) < 4:
        print "Correct use: " + sys.argv[0] + " archi_filename  startTime_str ('Y-m-d H:M:S')  endTime_str ('Y-m-d H:M:S')  dest_dir [hdb/tdb]"
        sys.exit(0)

    archi_filename = sys.argv[1]
    startTime_str = sys.argv[2]
    endTime_str = sys.argv[3]
    dest_dir = sys.argv[4]

    
    db = "TDB"
    if len(sys.argv) == 6 and (sys.argv[5] == "hdb" or sys.argv[5] == "HDB"):
        extractor = DeviceProxy("archiving/hdb/hdbextractor.1")
        db = "HDB"
    else:
        extractor = DeviceProxy("archiving/tdb/tdbextractor.1")
    #print db #debug


    extractor.set_timeout_millis(100000)

    # TEST 1 -------------------------------------------
    attr_list = reading_txtac.attrs_fromDevList(archi_filename)
    #print attr_list # DEBUG
    date_string = startTime_str[:10]
    startHour_string = startTime_str[11:].replace(':', '.')
    endHour_string = endTime_str[11:].replace(':', '.')
    
    baseOutputName = dest_dir + os.sep + date_string + '_' +  startHour_string + '-' + endHour_string + '_'
    
    extractData_fromTimeToTime( attr_list, startTime_str, endTime_str, extractor, baseOutputName)
  
         
    # TEST 2 -------------------------------------------
    #extractData_archivedPeriods(archi_filename,  startTime_str, endTime_str, extractor, dest_dir)
 		

