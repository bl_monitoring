#!/usr/bin/env python

import sys, os, time


##############################################
def sort_by_archiModes( attrs_archiModes_dict ):


    attrs_archiModes = []
    for a in attrs_archiModes_dict.keys():
        attrs_archiModes.append([a] + attrs_archiModes_dict[a])

    ac_byModes = [] # ac as archiving configuration
    
    "Initialization"
    attrs = [ attrs_archiModes[0][0] ]
    ac_byModes.append([ attrs_archiModes[0][1:], attrs ] )

    
    for i in range (1, len(attrs_archiModes)):

        attr_archiMode_i = attrs_archiModes[i]

        curr_attr = attr_archiMode_i[0]
        curr_mode =  attr_archiMode_i[1:]
        seen_mode = 0
        
        j = 0
        while  j in range (len(ac_byModes)) and not seen_mode:
            
            if curr_mode == ac_byModes[j][0]:
                seen_mode = 1
                ac_byModes[j][1].append(curr_attr)
            j = j +1

        if not seen_mode:
            attrs = [curr_attr]
            ac_byModes.append([curr_mode, attrs])
                    
    #print ac_byModes # debug
    return ac_byModes


##############################################
##def sort_by_archiModes( attrs_archiModes ):


##    ac_byModes = [] # ac as archiving configuration
    
##    "Initialization"
##    attrs = [ attrs_archiModes[0][0] ]
##    ac_byModes.append([ attrs_archiModes[0][1:], attrs ] )

    
##    for i in range (1, len(attrs_archiModes)):

##        attr_archiMode_i = attrs_archiModes[i]

##        curr_attr = attr_archiMode_i[0]
##        curr_mode =  attr_archiMode_i[1:]
##        seen_mode = 0
        
##        j = 0
##        while  j in range (len(ac_byModes)) and not seen_mode:
            
##            if curr_mode == ac_byModes[j][0]:
##                seen_mode = 1
##                ac_byModes[j][1].append(curr_attr)
##            j = j +1

##        if not seen_mode:
##            attrs = [curr_attr]
##            ac_byModes.append([curr_mode, attrs])
                    
##    #print ac_byModes # debug
##    return ac_byModes







#################################################
def ac_byModes_fromDevFiles( devAC_filenames ):


    splitted_lines = []
    for f in range (len(devAC_filenames)):
        
        f_lines = open(devAC_filenames[f]).readlines()
        
        "The 1st  line is supposed to contain the device name"
        "Removing '\n' at the end of the string"
        dev_name = f_lines[0].split()[0] 

        for i in range (1, len(f_lines)):
            splitted_f_line = f_lines[i].split()
            
            "Skipping the empty lines"
            if( len(splitted_f_line) != 0  and f_lines[i][0] != "#"):
                "Full attr name = device name + attr name"
                splitted_f_line[0] = dev_name + '/' + splitted_f_line[0]
                splitted_lines.append(splitted_f_line)


    return splitted_lines



############################
if  __name__ == '__main__':

    if len(sys.argv) != 2:
        print "File name expected"
        sys.exit(0)

    #print ac_byModes_fromDevList(sys.argv[1])
    filenames = []
    filenames.append(sys.argv[1])
    print ac_byModes_fromDevFiles(filenames)
