#!/usr/bin/env python

##########################
import sys, os, time

from bl_instance.build_bl import *

from archiving import *
##############################




##############################################################################################
if __name__ == '__main__':


    if len(sys.argv) < 3:
        print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   ELEM_NICKNAME   [tdb/hdb]\n"
        sys.exit(0)

    action = sys.argv[1]

    nicknames_elems =  dict([(elem.nickname, elem) for elem in beam_path + continuous_env])
    elem_nickname =  sys.argv[2]
    if not elem_nickname in nicknames_elems.keys():
        print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   ELEM_NICKNAME   [tdb/hdb]\n"
        print "Invalid ELEM_NICKNAME"
        sys.exit(0)

    db = 'Tdb'
    if len(sys.argv) == 4:
        db = sys.argv[3] 

    bl_elem = nicknames_elems[elem_nickname]

    if bl_elem.params:
        attrs_archiModes = dict( [(p.attr_fullName, p.archiving_mode.split())
                                  for p  in bl_elem.params] )
    elif isinstance(bl_elem, beam_stopper):
        attrs_archiModes = dict( [(bl_elem.beamAccess_param.attr_fullName,
                                   bl_elem.beamAccess_param.archiving_mode.split())] )
    else:
        print 'coucou'
        sys.exit(0)

    archiving(attrs_archiModes, action,  db)

