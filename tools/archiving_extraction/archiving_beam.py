#!/usr/bin/env python
########################
import sys, os, time

from bl_instance.build_bl import *

from archiving import *
########################


##############################################################################################
if __name__ == '__main__':


    if len(sys.argv) < 2:
        print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   [BEAM_MONITOR_ID]   [tdb/hdb]\n"
        sys.exit(0)

    action = sys.argv[1]

    nicknames_elems =  dict([(elem.nickname, elem) for elem in beam_path])

    if len(sys.argv) >= 3:
    
        monitor_nickname =  sys.argv[2]
        if not monitor_nickname in nicknames_elems.keys():
            print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   BEAM_MONITOR_ID   [tdb/hdb]\n"
            print "Invalid BEAM_MONITOR_ID"
            sys.exit(0)

        monitor = nicknames_elems[monitor_nickname]
        monitor.define_blSteadiness(beam_path)

        all_params = monitor.blSteadiness_params + monitor.params

    else:
        all_params = []
        for elem in beam_path:
            all_params += elem.params
            if isinstance(bl_elem, beam_stopper):
                all_params += elem.beamAccess_param
            

    attrs_archiModes = dict( [(p.attr_fullName, p.archiving_mode.split()) for p  in all_params] )


    db = 'Tdb'
    if len(sys.argv) == 4:
        db = sys.argv[3] 


    archiving(attrs_archiModes, action,  db)

