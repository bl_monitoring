#!/usr/bin/env python
##################################
import os, sys, time


from PyTango import *


from bl_instance.build_bl import *

from tools.generic_utils.time_formats  import *
from tools.generic_utils.dataPortioning_byTime  import *


from tools.data_processing.timePortioning_byData import *

from extraction import *
#################################




###############################################
startShift_hour = '07:00:00'
endShift_hour = '07:00:00'

extraPeriod_minLength_sec = 60*5 # = 5 min
EXTRAPERIOD_MAXLENGTH_SEC = 60*60*3 # = 3 hours
###############################################



############################
if __name__ == '__main__':


    if len(sys.argv) < 3:
        print "Correct use: " + sys.argv[0] + " startDate_str ('Y-m-d')  dest_dir  [hdb/tdb]"
        sys.exit(0)

    startDate_str = sys.argv[1]
    
    dest_dir = sys.argv[2]
    if not os.path.exists(dest_dir):
        print "The directory ", dest_dir, " does not exist"
        sys.exit(0)
    if dest_dir[-1] == os.sep:
        dest_dir = dest_dir[:-1]

    
    db = "TDB"
    if len(sys.argv) == 4 and (sys.argv[3] == "hdb" or sys.argv[3] == "HDB"):
        extractor = DeviceProxy("archiving/hdb/hdbextractor.1")
        db = "HDB"
    else:
        extractor = DeviceProxy("archiving/tdb/tdbextractor.1")
    #print db #debug


    extractor.set_timeout_millis(100000)
    # ----------------------------------------------------------------------------------------------

    startShift_str = startDate_str +' '+ startShift_hour
    startShift_sec = str2sec(startShift_str)

    startShift_hour_sec =  hour2sec(startShift_hour)
    endShift_hour_sec =  hour2sec(endShift_hour)
    
    if startShift_hour_sec >= endShift_hour_sec:
        startShift_date_sec = time.mktime(time.strptime(startDate_str, "%Y-%m-%d"))
        endShift_date_sec = startShift_date_sec + hour2sec("24:00:00")

    endShift_sec = endShift_date_sec + endShift_hour_sec
    endShift_str = sec2str(endShift_sec) 


    shift_str = (startShift_str, endShift_str)

##    print startShift_str # debug
##    print endShift_str # debug
    # ----------------------------------------------------------------------------------------------
    
    # INIT ---
    extractionPeriods_sec = [(startShift_sec, endShift_sec)]
    extractionPeriods_str = periodsSec2periodsStr(extractionPeriods_sec)
    steadyPeriods_str = extractionPeriods_str

    #beam_monitors = []
    #############################    Main loop    #########################

    for elem in beam_path:
        if isinstance(elem, beam_monitor)and not isinstance(elem, extractable_beamMonitor):
            monitor = elem # just a notation for a better readability
            print '\nMonitor: ', monitor.nickname
            
            monitor.define_blSteadiness(beam_path)

            # ------------------------------------------
            for param in monitor.beamAccess_params:
                print '\nBeam access: ', param.name # debug

                new_extractionPeriods_str = []

                param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                       shift_str,  extractor, dest_dir )
                #print len(param_data) # debug
                if len(param_data) == 0:
                    print "No data for ", param.name
                    sys.exit(0)
                    #subPeriods_str =  takeWholeOrEmpty_extendingExtraction(param, time_interval, extractor, dest_dir)

                # split param_data following the extractionPeriods ---
                data_portions = split_paramData( param_data, extractionPeriods_str)
                # in the loop over the extractionPeriods call get_extraPeriods for each data portion
                # and update new_extractionPeriods_str ---
                for i in range (len(data_portions)):
                    
                    if len(data_portions[i]) == 0:
                        subPeriods_str =  takeWholeOrEmpty_extendingTime(param, extractionPeriods_str[i], param_data)
                    else:
                        subPeriods_str = get_extraPeriods(param, data_portions[i], extractionPeriods_str[i],
                                                          extraPeriod_minLength_sec)

                    new_extractionPeriods_str += subPeriods_str
                    #print new_extractionPeriods_str # debug

                extractionPeriods_str = new_extractionPeriods_str
                print extractionPeriods_str # debug

                #sys.exit(0) #####################
                
            steadyPeriods_str = extractionPeriods_str
            #print steadyPeriods_str # debug

            # ------------------------------------------
            for param in monitor.blSteadiness_params:
                    print '\nSteadiness: ', param.name # debug
                    
##                    for time_interval in extractionPeriods_str:

##                        param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
##                                                               time_interval,  extractor, dest_dir )

                    param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                           shift_str,  extractor, dest_dir )

            # ------------------------------------------
##            print 'coucou'
##            sys.exit(0)
            for param in monitor.params:
                print '\n', param.name # debug
                
                for steady_period in steadyPeriods_str:
                    shortSteadyPeriods_str = split_longInterval(steady_period, EXTRAPERIOD_MAXLENGTH_SEC)
                    
                    for short_steadyPeriod in shortSteadyPeriods_str:
                        
                        param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                               short_steadyPeriod,  extractor, dest_dir )
            # -------------------------------------------------------
            
    print 'beam_path extraction done'

    
    env_dir = dest_dir + os.sep +'environment'
    if not os.path.exists(env_dir):
        os.mkdir(dest_dir + os.sep + 'environment')
        
    for elem in continuous_env:
        print '\nContinuous env: ', elem.nickname
        
        for param in elem.params:
            print '\n', param.name # debug
            
            shortPeriods_str = split_longInterval(shift_str, EXTRAPERIOD_MAXLENGTH_SEC)
                    
            for short_period in shortPeriods_str:
                param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                       short_period,  extractor, env_dir )
