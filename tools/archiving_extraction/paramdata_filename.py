#!/usr/bin/env python
#
import os, sys, time

from tools.generic_utils.time_formats  import *

####################################
def timeInterval2paramdataFilename(attr_fullname, timeInterval_str):

    # timeInterval_str = ('Y-m-d H:M:S', 'Y-m-d H:M:S')

    date_str = timeInterval_str[0][8:10]
    startHour_str = timeInterval_str[0][11:].replace(':', '.')
    endHour_str = timeInterval_str[1][11:].replace(':', '.')

    dev_name = attr_fullname.split('/')[-2]
    attr_shortName = attr_fullname.split('/')[-1]

    return date_str +'_'+ startHour_str + '-' + endHour_str + '_' + dev_name + '.' + attr_shortName


####################################
def filename2timeInterval(paramdata_fullpath):

    splitted_path = paramdata_fullpath.split('/')

    startDate_str = splitted_path[-4] + '-'+ splitted_path[-3] + '-'+ splitted_path[-2]   
    
    filename = splitted_path[-1]

    startHour_str = filename[3:11].replace('.', ':')
    endHour_str = filename[12:20].replace('.', ':')

    startTime_str = startDate_str +' '+ startHour_str

    startTime_hour_sec =  hour2sec(startHour_str)
    endTime_hour_sec =  hour2sec(endHour_str)
    
    if startTime_hour_sec >= endTime_hour_sec:
        startTime_date_sec = time.mktime(time.strptime(startDate_str, "%Y-%m-%d"))
        endTime_date_sec = startTime_date_sec + hour2sec("24:00:00")

    endTime_sec = endTime_date_sec + endTime_hour_sec
    endTime_str = sec2str(endTime_sec) 


    return  (startTime_str, endTime_str)

##################################################################################################
if __name__ == '__main__':


    if len(sys.argv) < 4:
        print "Correct use: " + sys.argv[0] + " filename "
        sys.exit(0)

    filename = sys.argv[1]
