#!/usr/bin/env python
#
import os, sys, time

from PyTango import *



#########################
def hour2sec(hour_str):
    splitted_hour = hour_str.split(':')
    return float(splitted_hour[0])*3600 + float(splitted_hour[1])*60 + float(splitted_hour[2])


#########################
def str2sec(str):
    return time.mktime(time.strptime(str, "%Y-%m-%d %H:%M:%S"))


#######################################
def periodsStr2periodsSec(periods_str):
    periods_sec = []

    for p in range (len(periods_str)):

        startPeriod_sec = str2sec(periods_str[p][0]) 
        endPeriod_sec = str2sec(periods_str[p][1]) 

        periods_sec.append( (startPeriod_sec, endPeriod_sec) )

    return periods_sec


#################
def sec2str(sec):
    return time.strftime( "%Y-%m-%d %H:%M:%S", time.localtime(sec) )


########################################
def periodsSec2periodsStr(periods_sec):
    periods_str = []

    for p in range (len(periods_sec)):

        startPeriod_str = sec2str(periods_sec[p][0]) 
        endPeriod_str = sec2str(periods_sec[p][1]) 

        periods_str.append( (startPeriod_str, endPeriod_str) )

    return periods_str



####################################################
def select_longIntervals(intervals_sec, minLength_sec):

    long_intervals = []
    for i in intervals_sec:
        if i[1] - i[0] >= minLength_sec:
            long_intervals.append(i)

    return long_intervals

####################################################
def split_longInterval(interval_str, maxLength_sec):

    interval_sec = periodsStr2periodsSec([interval_str])[0]
    interval_length = interval_sec[1] - interval_sec[0]
    
    if interval_length <= maxLength_sec:
        return [interval_str]

    times = int(interval_length/maxLength_sec)
    rest = interval_length - times*maxLength_sec

    shorter_intervals = []
    for i in range (times-1):
        shorter_intervals.append((interval_sec[0] + maxLength_sec*i, interval_sec[0]+ (i+1)*maxLength_sec))
        
    if rest < float(maxLength_sec)/3:
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*(times-1), interval_sec[0]+ maxLength_sec*times+ rest))
    else:
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*(times-1), interval_sec[0]+ maxLength_sec*times))
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*times, interval_sec[0]+ maxLength_sec*times+ rest))

    return periodsSec2periodsStr(shorter_intervals)


#################################################################################
def get_okIntervals_indxs(param, param_data):

    if len(param_data) == 0:
        return [] # it is not really correct but we are not supposed to get here 
      
    #timeInterval_sec = periodsStr2periodsSec([timeInterval_str])[0]

    curr_startInx = -1
    curr_endInx = -1
    ok_intervals = []
    was_ok = False

    for i in range(len(param_data)):

        ok = param.is_ok(param_data[i][1])
        if ok and not was_ok:
            curr_startInx = i
            was_ok = True
        elif not ok and was_ok:
            curr_endInx = i-1
            was_ok = False
            ok_intervals.append((curr_startInx, curr_endInx))
            
    #if curr_startInx != -1 and curr_endInx == -1:
    if curr_startInx > curr_endInx:
        ok_intervals.append((curr_startInx, len(param_data)-1))

    #print ok_intervals # debug

##    if len(ok_intervals) == 0:
##        return []

    return ok_intervals


#################################################################################
def get_extraPeriods(param, param_data, timeInterval_str, minLength_sec = 0):


    okIntervals_indxs = get_okIntervals_indxs(param, param_data)

    if len(okIntervals_indxs) == 0:
        return []

    subIntervals_sec = []

    timeInterval_sec = periodsStr2periodsSec([timeInterval_str])[0]
    
    if okIntervals_indxs[0][0] == 0:
        first_start = timeInterval_sec[0]
    else: first_start = param_data[okIntervals_indxs[0][0]][0]

    #print sec2str(first_start) # debug
    
    if okIntervals_indxs[-1][1] == len(param_data)-1:
        last_end = timeInterval_sec[1]
    else: last_end = param_data[okIntervals_indxs[-1][1]][0]

    #print sec2str(last_end) # debug

    if len(okIntervals_indxs) == 1:
        subIntervals_sec.append( (first_start,  last_end) )
    else:
        subIntervals_sec.append((first_start,  param_data[okIntervals_indxs[0][1]][0]))
        for i in range(1, len(okIntervals_indxs)-1):
            subIntervals_sec.append((param_data[okIntervals_indxs[i][0]][0],  param_data[okIntervals_indxs[i][1]][0]))
        subIntervals_sec.append((param_data[okIntervals_indxs[-1][0]][0],  last_end))


    
    return periodsSec2periodsStr( select_longIntervals(subIntervals_sec, minLength_sec) )


#################################################################################
def get_steadyPeriods(param, param_data, timeInterval_str, minLength_sec = 0):

    #subIntervals_sec = periodsStr2periodsSec([timeInterval_str])

    var_data = param.get_variation(param_data, periodsStr2periodsSec([timeInterval_str])[0])
    
    okIntervals_indxs = get_okIntervals_indxs(param, var_data)

    subIntervals_sec = []

    for i in okIntervals_indxs:
        subIntervals_sec.append( (var_data[i[0]][0][0], var_data[i[1]][0][1]) )

    return periodsSec2periodsStr( select_longIntervals(subIntervals_sec, minLength_sec) )



#################################################################################
def takeWholeOrEmpty_extendingExtraction(param, timeInterval_str, extractor, dest_dir):
    # ones called it would not work as we don't know startShift_sec !!!!!!!
    # so let's take all
    print "WARNING: data_portionning.takeWholeOrEmpty_extendingExtraction called"
    print param.name
    print timeInterval_str
    
    return [timeInterval_str]
    

    if ( str2sec(timeInterval_str[1]) - str2sec(timeInterval_str[0]) <=
             float(param.archiving_period)/1000 ):
            
            extended_timeInterval_sec = ( str2sec(timeInterval_str[1])- float(param.archiving_period)/1000,
                                          str2sec(timeInterval_str[1]) )

            
            
            param_data = extractAttr_relativeTime( param.attr_fullName, startShift_sec,
                                                   periodsSec2periodsStr([extended_timeInterval_sec])[0],
                                                   extractor, dest_dir )
            
            if len(param_data) == 0:
                print "No data for ", param.attr_fullName, " in the ", db, \
                      " for the period ", periodsSec2periodsStr([extended_timeInterval_sec])[0]
                sys.exit(0)

            if param.is_ok(param_data[-1][1]):
                return [timeInterval_str]      # whole interval
            else:
                return []                      # empty list
    else:
        print "No data for ", param.attr_fullName, " in the ", db, \
              " for the period ", timeInterval_str
        sys.exit(0)


##################################################
def get_dataPortion(data, timeInterval_sec):

    if timeInterval_sec[0] < data[0][0] or timeInterval_sec[1] > data[len(data)-1][0]:
        print "Invalid parameters in data_portionnig.get_dataPortion"
        sys.exit(0)

    i =0
    while(data[i][0] < timeInterval_sec[0]):
        i +=1
    start_inx = i
    
    while(data[i][0] <= timeInterval_sec[1]):
        i += 1
    end_inx = i

    return data[start_inx:end_inx]

    
#################################################################################
def takeWholeOrEmpty_extendingTime(param, timeInterval_str, param_data):

    if ( str2sec(timeInterval_str[1]) - str2sec(timeInterval_str[0]) <=
             float(param.archiving_period)/1000 ):
            
            extended_timeInterval_sec = ( max( str2sec(timeInterval_str[1])- float(param.archiving_period)/1000,  param_data[0][0]),
                                          str2sec(timeInterval_str[1]) )

            # take  the corresponding portion of param_data
            data_portion = get_dataPortion(param_data, extended_timeInterval_sec)
            
            if len(data_portion) == 0:
                print "No data for ", param.attr_fullName, " in the ", db, \
                      " for the period ", periodsSec2periodsStr([extended_timeInterval_sec])[0]
                sys.exit(0)

            if param.is_ok(data_portion[-1][1]):
                return [timeInterval_str]      # whole interval
            else:
                return []                      # empty list
    else:
        print "No data for ", param.attr_fullName, " in the ", db, \
              " for the period ", timeInterval_str
        sys.exit(0)


##############################################
def split_paramData(param_data, periods_str):

    data_piecewise = []

    periods_sec = periodsStr2periodsSec(periods_str)

    i =0
    
    for period in periods_sec:
        #print period # debug
        
##        if period[0] < param_data[0][0] or period[1] > param_data[len(param_data)-1][0]:
##            print "Invalid parameters in data_portionnig.split_paramData"
##            sys.exit(0)

        if period[0] < param_data[0][0] or period[1] > param_data[len(param_data)-1][0]:
            return [param_data]
        
        while(param_data[i][0] < period[0]):
            i +=1
        start_inx = i

        while(param_data[i][0] <= period[1]):
            i += 1
        end_inx = i

        data_piecewise.append(param_data[start_inx:end_inx])

    return data_piecewise
    
####################################################
##def split_longPeriod( startTime_str, endTime_str ):

##    #start_time = str2time(startTime_str)
##    #end_time = str2time(endTime_str)
##    period_length = str2time(endTime_str) - str2time(startTime_str)
    
##    fourHours = 4*3600
##    threeHours = 3*3600
    
##    sub_periods = []
##    if period_length <= fourHours:
##        sub_periods.append( (startTime_str, endTime_str) )
##    else:
##        nb_perodsBy4hours = int(period_length)/fourHours
##        rest4 = period_length - nb_perodsBy4hours*fourHours
        
##        nb_perodsBy3hours = int(period_length)/threeHours
##        rest3 = period_length - nb_perodsBy3hours*threeHours


##        startTime_i_tuple = time.strptime(startTime_str, "%Y-%m-%d %H:%M:%S")
##        endTime_i_tuple = startTime_i_tuple
        
##        if fourHours-rest4 < threeHours-rest3:
##            endTime_i_tuple[3] += 4 
##            for i in range (1, nb_perodsBy4hours):
##                startTime_i_str = strftime("%Y-%m-%d %H:%M:%S", startTime_i_tuple)
##                endTime_i_str = strftime("%Y-%m-%d %H:%M:%S", endTime_i_tuple)
##                sub_periods.append((startTime_i_str, endTime_i_str))
##                startTime_i_tuple[3] += 4
##                endTime_i_tuple[3] += 4
##            startTime_i_str = strftime("%Y-%m-%d %H:%M:%S", startTime_i_tuple)
##            sub_periods.append((startTime_i_str, endTime_str))
##        else:
##            endTime_i_tuple[3] += 3 
##            for i in range (1, nb_perodsBy3hours):
##                startTime_i_str = strftime("%Y-%m-%d %H:%M:%S", startTime_i_tuple)
##                endTime_i_str = strftime("%Y-%m-%d %H:%M:%S", endTime_i_tuple)
##                sub_periods.append((startTime_i_str, endTime_i_str))
##                startTime_i_tuple[3] += 3
##                endTime_i_tuple[3] += 3
##            startTime_i_str = strftime("%Y-%m-%d %H:%M:%S", startTime_i_tuple)
##            sub_periods.append((startTime_i_str, endTime_str))
    
##    return sub_periods

##################################################################################################

##################################################################################################
if __name__ == '__main__':


    if len(sys.argv) < 4:
        print "Correct use: " + sys.argv[0] + " archi_filename  startTime_str ('Y-m-d H:M:S')  endTime_str ('Y-m-d H:M:S')  dest_dir [hdb/tdb]"
        sys.exit(0)

    archi_filename = sys.argv[1]
    startTime_str = sys.argv[2]
    endTime_str = sys.argv[3]
    dest_dir = sys.argv[4]

    
    db = "TDB"
    if len(sys.argv) == 6 and (sys.argv[5] == "hdb" or sys.argv[5] == "HDB"):
        extractor = DeviceProxy("archiving/hdb/hdbextractor.1")
        db = "HDB"
    else:
        extractor = DeviceProxy("archiving/tdb/tdbextractor.1")
    #print db #debug


    extractor.set_timeout_millis(100000)

    # TEST 1 -------------------------------------------
    attr_list = reading_txtac.attrs_fromDevList(archi_filename)
    #print attr_list # DEBUG
    date_string = startTime_str[:10]
    startHour_string = startTime_str[11:].replace(':', '.')
    endHour_string = endTime_str[11:].replace(':', '.')
    
    baseOutputName = dest_dir + os.sep + date_string + '_' +  startHour_string + '-' + endHour_string + '_'
    
    extractData_fromTimeToTime( attr_list, startTime_str, endTime_str, extractor, baseOutputName)
  
         
    # TEST 2 -------------------------------------------
    #extractData_archivedPeriods(archi_filename,  startTime_str, endTime_str, extractor, dest_dir)
 		

