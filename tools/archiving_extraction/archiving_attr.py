#!/usr/bin/env python

##########################
import sys, os, time


from bl_instance.build_bl import *

from archiving import *
########################





############################################################################
if __name__ == '__main__':


    if len(sys.argv) < 3:
        print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   ATTR_FULLNAME   [tdb/hdb]\n"
        sys.exit(0)

    action = sys.argv[1]
    attr_fullname = sys.argv[2]

    if action == 'start':
        attr_found = False
        for elem in beam_path + continuous_env:
            
            attrs_params = dict( [(p.attr_fullName, p) for p  in (elem.params)] )
            
            if attr_fullname in attrs_params.keys():
                archi_mode = attrs_params[attr_fullname].archiving_mode.split()
                attr_found = True
                break
            
        if not attr_found:
            print "\nCorrect use: " + sys.argv[0] + "   start/stop/check   ATTR_FULLNAME   [tdb/hdb]\n"
            print "Invalid ATTR_FULLNAME"
            sys.exit(0)

    db = 'Tdb'
    if len(sys.argv) == 4:
        db = sys.argv[3] 

    if  action == 'start':
        attrs_archiModes = dict( [(attr_fullname, archi_mode)] )
    else:
        attrs_archiModes = dict( [(attr_fullname, ['MODE_P', '1000'])] )
    

    archiving(attrs_archiModes, action,  db)

