#!/usr/bin/env python
#
import os, sys, time
from time_formats import *


##############################################
def shift_time(intervals_sec, shift_sec):

    shifted_intervals = []
    
    for i in intervals_sec:
        l = i[0] + shift_sec
        r = i[1] + shift_sec
        shifted_intervals.append((l,r))

    return shifted_intervals
    
##############################################
def rel2abs(intervals_sec, zeroTime_str):

    shifted_intervals_sec = shift_time(intervals_sec, str2sec(zeroTime_str))

    return periodsSec2periodsStr(shifted_intervals_sec)

##############################################
def abs2rel(intervals_sec, zeroTime_str):

    shifted_intervals_sec = shift_time(intervals_sec, -str2sec(zeroTime_str))

    return shifted_intervals_sec


####################################################
def select_longIntervals(intervals_sec, minLength_sec):

    long_intervals = []
    for i in intervals_sec:
        if i[1] - i[0] >= minLength_sec:
            long_intervals.append(i)

    return long_intervals


####################################################
def split_longInterval(interval_str, maxLength_sec):

    interval_sec = periodsStr2periodsSec([interval_str])[0]
    interval_length = interval_sec[1] - interval_sec[0]
    
    if interval_length <= maxLength_sec:
        return [interval_str]

    times = int(interval_length/maxLength_sec)
    rest = interval_length - times*maxLength_sec

    shorter_intervals = []
    for i in range (times-1):
        shorter_intervals.append((interval_sec[0] + maxLength_sec*i, interval_sec[0]+ (i+1)*maxLength_sec))

        
    if rest < float(maxLength_sec)/3:
        
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*(times-1),
                                  interval_sec[0]+ maxLength_sec*times+ rest))
        
    else:
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*(times-1),
                                  interval_sec[0]+ maxLength_sec*times))
        
        shorter_intervals.append((interval_sec[0]+ maxLength_sec*times,
                                  interval_sec[0]+ maxLength_sec*times+ rest))

    return periodsSec2periodsStr(shorter_intervals)

