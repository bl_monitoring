#!/usr/bin/env python
#
import os, sys, time


#########################
def hour2sec(hour_str):
    splitted_hour = hour_str.split(':')
    return float(splitted_hour[0])*3600 + float(splitted_hour[1])*60 + float(splitted_hour[2])


#########################
def str2sec(str):
    return time.mktime(time.strptime(str, "%Y-%m-%d %H:%M:%S"))


#################
def sec2str(sec):
    return time.strftime( "%Y-%m-%d %H:%M:%S", time.localtime(sec) )



#######################################
def periodsStr2periodsSec(periods_str):
    periods_sec = []

    for p in range (len(periods_str)):

        startPeriod_sec = str2sec(periods_str[p][0]) 
        endPeriod_sec = str2sec(periods_str[p][1]) 

        periods_sec.append( (startPeriod_sec, endPeriod_sec) )

    return periods_sec


########################################
def periodsSec2periodsStr(periods_sec):
    periods_str = []

    for p in range (len(periods_sec)):

        startPeriod_str = sec2str(periods_sec[p][0]) 
        endPeriod_str = sec2str(periods_sec[p][1]) 

        periods_str.append( (startPeriod_str, endPeriod_str) )

    return periods_str

