#!/usr/bin/env python
#
import os, sys, time

from tools.generic_utils.time_formats import *




##################################################
def get_dataPortion(data, timeInterval_sec):

    if timeInterval_sec[0] < data[0][0] or timeInterval_sec[1] > data[len(data)-1][0]:
        print "Invalid parameters in data_portionnig.get_dataPortion"
        sys.exit(0)

    i =0
    while(data[i][0] < timeInterval_sec[0]):
        i +=1
    start_inx = i
    
    while(data[i][0] <= timeInterval_sec[1]):
        i += 1
    end_inx = i

    return data[start_inx:end_inx]


#################################################
def split_paramData(param_data, periods_str):

    data_piecewise = []

    periods_sec = periodsStr2periodsSec(periods_str)

    i =0
    
    for period in periods_sec:
        #print period # debug
        
##        if period[0] < param_data[0][0] or period[1] > param_data[len(param_data)-1][0]:
##            print "Invalid parameters in data_portionnig.split_paramData"
##            sys.exit(0)

        if period[0] < param_data[0][0] or period[1] > param_data[len(param_data)-1][0]:
            return [param_data]
        
        while(param_data[i][0] < period[0]):
            i +=1
        start_inx = i

        while(param_data[i][0] <= period[1]):
            i += 1
        end_inx = i

        data_piecewise.append(param_data[start_inx:end_inx])

    return data_piecewise
    
