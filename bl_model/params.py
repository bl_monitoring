#!/usr/bin/env python
#######################################
import os

#import PyTango
###############

####################################################
ACTUATOR_ARCHIVING_PERIOD = 900000 # ms = 15min
BEAM_ARCHIVING_PERIOD = 1000 # ms
#####################################################


###########
class param:
    
    def __init__(self, _attr_fullName):

        self.attr_fullName = _attr_fullName

        splitted_attr_fullName =  self.attr_fullName.split('/')
        self.name = splitted_attr_fullName[-2] + '/'+ splitted_attr_fullName[-1] # device_slash_attr

    def get_attrName(self):
        return self.name.split('/')[1]

    def get_devName(self):
        return self.name.split('/')[0]

        
###############################
class steadiness_param(param):
    
    def __init__(self, _attr_fullName, _delta  = 0, _ok_values = ['const'],
                 _archiving_period = ACTUATOR_ARCHIVING_PERIOD):

        param.__init__(self, _attr_fullName)
        
        self.delta = _delta
        
        self.archiving_period = _archiving_period

        if self.delta < 0:
            self.archiving_mode = ( 'MODE_P '+ str(self.archiving_period) + '  MODE_D ' + str(BEAM_ARCHIVING_PERIOD) )
        else:
            self.archiving_mode = ('MODE_P '+ str(self.archiving_period) + 
                                   '  MODE_A ' + str(BEAM_ARCHIVING_PERIOD) + ' ' + str(self.delta) + ' ' + str(self.delta) + ' false')

        self.ok_values = _ok_values

    # ------------------------------------------------------
    def get_variation(self, param_data, timeInterval_sec):

        n = len(param_data)

        var_data = []
        
        #x_0 = timeInterval_sec[0]+float(param_data[0][0]-timeInterval_sec[0])/2
        x_0 = (timeInterval_sec[0], param_data[0][0])
        var_data.append( ( x_0, 0) )
        
        for i in range( 1, n ):
            #x_i = param_data[i-1][0]+ float(param_data[i][0]-param_data[i-1][0])/2
            x_i = (param_data[i-1][0], param_data[i][0])
            y_i = abs( param_data[i][1]-param_data[i-1][1] )
            var_data.append( ( x_i, y_i) )
            
        #x_n = param_data[n-1][0] + float(timeInterval_sec[1]-param_data[n-1][0])/2
        x_n = (param_data[n-1][0], timeInterval_sec[1])
        var_data.append( ( x_n, 0) )
            
        return var_data

    # -----------------------
    def is_ok(self, _val):

        if self.ok_values != ['const'] and _val in self.ok_values:
            return True
        
        elif self.ok_values == ['const']:
            return (self.delta - _val > 0) # _val = abs(param.val[i]-param.val[i-1])
        
        return False

    # -----------------------
    def set_delta(self, _val):
        self.delta = _val

    # -----------------------
    def round_val(self, _val):
        "Not implemented yet"
        return _val


###############################
class monitored_param(param):
        
    def __init__(self, _attr_fullName, _archiving_period = BEAM_ARCHIVING_PERIOD):

        param.__init__(self, _attr_fullName)
        
        self.archiving_period = _archiving_period
        self.archiving_mode = 'MODE_P '+ str(self.archiving_period)

##    def set_archiPeriod(_archiving_period):
##        self.archiving_period = _archiving_period
        



