#!/usr/bin/env python
######################
import os

from bl_instance.build_bl import bl_instance_path
from params import *
####################

####################################################
ACTUATOR_ARCHIVING_PERIOD = 900000 # ms = 15min
BEAM_ARCHIVING_PERIOD = 1000 # ms

DEFAULT_CONFIG_PATH = bl_instance_path + os.sep + 'config'     # aiaiaiai !!!!!!!!! ????????????
#####################################################





############
class bl_elem:
############
    
    def __init__(self, _nickname, _vacuum_cell, _dev_group, _devices, _config_path = DEFAULT_CONFIG_PATH): # devGroups: 'EX', 'DT', 'OP' etc.  
        self.nickname = _nickname
        self.devices = _devices
        self.dev_group = _dev_group
        self.vacuum_cell = _vacuum_cell

##        self.full_devNames = []
##        for dev in self.devices:
##            self.full_devNames.append( self.vacuum_cell + '/' + self.dev_group + '/'+ dev)

        # self.archiving_mode = '' 
        
        self.params = []

        #filling the list of params
        config_path = _config_path
        config_list = os.listdir(config_path)

        for dev_name in self.devices:
            
            dev_subname = dev_name
            if '-' in dev_name:
                dev_subname = dev_name.split('-')[1]
                
            for filename in config_list:

                if filename in dev_subname.lower():

                    lines = open(config_path +'/'+ filename).readlines()
                    for l in lines:
                        sl = l.split()
                        if len(sl):
                            if sl[0][0] != '#':
                                attr_fullName = self.vacuum_cell +'/'+  self.dev_group +'/'+ dev_name+'/'+l.split()[0]
                                self.add_param(attr_fullName)

            
    def add_param(self, attr_fullName):
        #print self.short_name + ": bl_elem.add_param" # DEBUG
        self.params.append(param(attr_fullName))


    def print_params(self):
        for p in self.params:
            
            if isinstance(p, monitored_param):
                print p.name 
            elif isinstance(p, steadiness_param):
                print p.name + '\t' + str(p.delta)
        print
        
    def get_fullDevNames(self):
        full_devNames = []

        for dev in self.devices:
            full_devNames.append( self.vacuum_cell + '/' + self.dev_group + '/'+ dev)

        return full_devNames
    
            
#####################################       
class motorized_beamModifier(bl_elem):
# slits, monochromator, mirrors, attenuators... what else?

    def __init__(self, _nickname, _vacuum_cell, _dev_group, _devices, _common_delta, _config_path = DEFAULT_CONFIG_PATH):

        self.common_delta = _common_delta

        bl_elem.__init__(self, _nickname, _vacuum_cell, _dev_group, _devices)



    def add_param(self, attr_fullName, _delta = 0):

        #print self.short_name + ": motorized_beamModifier.add_param " + devSlashAttr# DEBUG
        
        delta = _delta
        if delta == 0:
            delta = self.common_delta

        self.params.append( steadiness_param(attr_fullName, delta) )
        

    def set_commonDelta(self, val):
        self.common_delta = val


#####################################################
class beam_stopper(motorized_beamModifier): # frontend, obx... what else?

    def __init__(self, _nickname, _beamAccess_attrFullname,  _beamAccess_attrVals, _common_delta = -1, _config_path = DEFAULT_CONFIG_PATH):

        self.nickname = _nickname
        
        splitted_attrName = _beamAccess_attrFullname.split('/')

        self.vacuum_cell = splitted_attrName[0]
        self.dev_group = splitted_attrName[1]
        self.devices = []
        self.params = []
        
        self.common_delta = _common_delta
        
        self.beamAccess_param = steadiness_param(_beamAccess_attrFullname, self.common_delta, _beamAccess_attrVals)

        
        
###########################        
class beam_monitor(bl_elem):

    def __init__(self, _nickname, _vacuum_cell, _dev_group, _devices, _config_path = DEFAULT_CONFIG_PATH):
        
        bl_elem.__init__(self, _nickname, _vacuum_cell, _dev_group, _devices)

        self.beamAccess_params = []
        self.blSteadiness_elems = []
        self.blSteadiness_params = []


    def add_param(self, attr_fullName):

        #print self.short_name + ": beam_monitor.add_param " + devSlashAttr# DEBUG
        self.params.append( monitored_param(attr_fullName) )


    def define_blSteadiness(self, beam_path):
        
        if not self in beam_path:
            print self.nickname + " is not found in the bl description"
            sys.exit(0)

        # Selects from beam_path all the motorized_beamModifiers preceeding this monitor
        # While none of their motors moves, the beam characteristics are not supposed to change,
        # that is why blSteadiness


        self_inx = beam_path.index(self)
        i = self_inx-1
        while ( i > 0 and
                (not isinstance(beam_path[i], beam_monitor)
                 or isinstance(beam_path[i], extractable_beamMonitor)) ):
            i -= 1

        if isinstance(beam_path[i], beam_monitor):
            precMonitor_inx = i
        else:
            precMonitor_inx = -1 # when self is the first beam_monitor
            
##        monitors_inxs = {}
##        for elem in beam_path:
##             if isinstance(elem, beam_monitor):
##                 monitors_inxs[elem] = beam_path.index(elem)
                 
        for elem in beam_path[precMonitor_inx+1:self_inx]:

            if isinstance(elem, beam_stopper):
                self.beamAccess_params.append(elem.beamAccess_param)
                    
            elif isinstance(elem, motorized_beamModifier):
                
                self.blSteadiness_elems.append(elem) # probably useless
                
                for param in elem.params:
                    self.blSteadiness_params.append(param)


                        
######################################################################
class extractable_beamMonitor(beam_monitor, beam_stopper):
# 
    
    def __init__(self, _nickname, _vacuum_cell, _dev_group, _devices, _isextracted_devSlashAttr):

        beam_monitor.__init__(self, _nickname, _vacuum_cell, _dev_group, _devices)

        self.beamAccess_param = self.vacuum_cell +'/'+ self.dev_group +'/'+_isextracted_devSlashAttr


    def add_param(self, devSlashAttr):

        #print self.short_name + ": extractable_beamMonitor.add_param " + devSlashAttr# DEBUG
        self.params.append( monitored_param(devSlashAttr) )



##########################################
class continuousEnvironment_elem(bl_elem):

    def __init__(self, _nickname, _vacuum_cell, _dev_group, _devices, _archiving_period = BEAM_ARCHIVING_PERIOD,
                 _config_path = DEFAULT_CONFIG_PATH):

        
        self.archiving_period = _archiving_period
        
        bl_elem.__init__(self, _nickname, _vacuum_cell, _dev_group, _devices)



    def add_param(self, attr_fullName):

        self.params.append( monitored_param(attr_fullName, self.archiving_period) )
        

